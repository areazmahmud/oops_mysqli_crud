<?php 
	include_once 'controllers/Book.php';
	$bk = new Book();
 ?>
 <!DOCTYPE html>
 <html>
 <head>
 	<title>CRUD MySQLi - Create</title>
 </head>
 <body>
 	 <a href="index.php">Home</a> | <a href="create.php">Create</a> | <a href="trash.php">Trash</a> 
 	<h2>Create Data</h2>
 	<?php 
 		if (isset($_POST['submit'])) {
 			$title  = $_POST['title'];
 			$author = $_POST['author'];

 			if (empty($title) || empty($author)) {
 				echo "<span style='color:red;font-size:20px'>Field must not be empty !</span>";
 			} else {
 				$bk->setData($title, $author);
 				if ($bk->create()) {
 					echo "<span style='color:green;font-size:20px'>Data Created Successfully.</span>";
 				}
 			}
 		}
 	 ?>
 	<form method="post">
 		<table>
 			<tr>
 				<td>Title:</td>
 				<td><input type="text" name="title"></td>
 			</tr>
 			<tr>
 				<td>Author:</td>
 				<td><input type="text" name="author"></td>
 			</tr>
 			<tr>
 				<td></td>
 				<td><input type="submit" name="submit" value="Create"></td>
 			</tr>
 		</table>
 	</form>
 </body>
 </html>