<?php 
	include_once 'controllers/Book.php';
	$bk = new Book();
 ?>
 <!DOCTYPE html>
 <html>
 <head>
 	<title>CRUD MySQLi - Update</title>
 </head>
 <body>
 	 <a href="index.php">Home</a> | <a href="create.php">Create</a> | <a href="trash.php">Trash</a> 
 	<h2>Update Data</h2>
 	<?php 
 		if (isset($_POST['submit'])) {
 			$id     = $_POST['id'];
 			$title  = $_POST['title'];
 			$author = $_POST['author'];

 			if (empty($title) || empty($author)) {
 				echo "<span style='color:red;font-size:20px'>Field must not be empty !</span>";
 			} else {
 				$bk->setData($title, $author);
 				if ($bk->update($id)) {
 					header('Location: index.php?msg='.urldecode('Data Updated Successfully.'));
 				}
 			}
 		}

 		if (isset($_GET['my_id'])) {
 			$my_id  = $_GET['my_id'];
 			$result = $bk->readById($my_id);
 			$data   = $result->fetch_assoc();
 		}
 	 ?>
 	<form method="post">
 		<table>
 			<input type="hidden" name="id" value="<?php echo $data['id']; ?>">
 			<tr>
 				<td>Title:</td>
 				<td><input type="text" name="title" value="<?php echo $data['title']; ?>"></td>
 			</tr>
 			<tr>
 				<td>Author:</td>
 				<td><input type="text" name="author" value="<?php echo $data['author']; ?>"></td>
 			</tr>
 			<tr>
 				<td></td>
 				<td><input type="submit" name="submit" value="Update"></td>
 			</tr>
 		</table>
 	</form>
 </body>
 </html>