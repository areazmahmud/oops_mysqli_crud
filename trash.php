<?php
include_once 'controllers/Book.php';
$bk = new Book();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>CRUD MySQLi - Display</title>
    </head>
    <body>
         <a href="index.php">Home</a> | <a href="create.php">Create</a> | <a href="trash.php">Trash</a> 
        <h2>All Book Record</h2>
        <?php
        if (isset($_GET['msg'])) {
            $msg = $_GET['msg'];
            echo "<span style='color:green;font-size:20px'>" . $msg . "</span>";
        }
        ?>
        <table border="1px">
            <tr>
                <th>Sl</th>
                <th>Title</th>
                <th>Author</th>
                <th>Actions</th>
            </tr>
            <?php
            
            if (isset($_GET['my_id'])) {
		$my_id = $_GET['my_id'];
//                $data = array(
//               'id' => $my_id,
//               'is_del' => 1     
//                );          
		$result = $bk->trash($my_id);
		if ($result) {
			header('Location: trash.php?msg='.urldecode('Successfully Go To Trash.'));
		}
	}
         
            $result = $bk->getBook();
            if ($result) {
                $i = 0;
                while ($alldata = $result->fetch_assoc()) {
                    $i++;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $alldata['title']; ?></td>
                        <td><?php echo $alldata['author']; ?></td>
                        <td>
                            <a href="emty_trash.php?my_id=<?php echo $alldata['id']; ?>"onclick="return confirm('Are you sure to delete data?');">Emty Trash</a> || 
                            <a href="restore.php?my_id=<?php echo $alldata['id']; ?>" >Restore</a> 
                        </td>
                    </tr>
                <?php }
            } ?>
        </table>
    </body>
</html>