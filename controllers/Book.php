<?php 
	include_once 'database/DB.php';
	class Book
	{
		private $db;
		private $table = "tbl_books";
		private $title;
		private $author;

		public function __construct()
		{
			$this->db = new DB();
		}

		public function index()
		{
			$query  = "SELECT * FROM $this->table  WHERE is_del = 0";
			$result = $this->db->select($query);
			return $result;
		}
                
                public function getBook()
		{
			$query  = "SELECT * FROM $this->table  WHERE is_del = 1";
			$result = $this->db->select1($query);
			return $result;
		}

		public function setData($title, $author) 
		{
			$this->title  = $title;
			$this->author = $author;
		}

		public function create()
		{
			$query = "INSERT INTO $this->table(title, author) VALUES('$this->title', '$this->author')";
			$insert_data = $this->db->insert($query);
			return $insert_data;
		}

		public function readById($my_id)
		{
			$query  = "SELECT * FROM $this->table WHERE id = '$my_id'";
			$result = $this->db->select($query);
			return $result;
		}

		public function update($id)
		{
			$query = "UPDATE $this->table SET title = '$this->title', author = '$this->author' WHERE id = '$id'";
			$update_data = $this->db->update($query);
			return $update_data;
		}

		public function trash($my_id)
		{
//                        $is_del ="";
			$query = "UPDATE $this->table SET is_del= 1 WHERE id = $my_id";
			$trash_data = $this->db->trash($query);
			return $trash_data;
		}
                
                
                public function restore($my_id)
		{
//                        $is_del ="";
			$query = "UPDATE $this->table SET is_del= 0 WHERE id = $my_id";
			$trash_data = $this->db->restore($query);
			return $trash_data;
		}
                
                public function emty_trash($my_id )
		{
			$query = "DELETE FROm $this->table WHERE id = '$my_id'";
			$delete_data = $this->db->emty_trash($query);
			return $delete_data;
		}
	}
 ?>