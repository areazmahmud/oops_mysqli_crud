<?php 
	include_once 'controllers/Book.php';
	$bk = new Book();
 ?>
 <!DOCTYPE html>
 <html>
 <head>
 	<title>CRUD MySQLi - Display</title>
 </head>
 <body>
 	 <a href="index.php">Home</a> | <a href="create.php">Create</a> | <a href="trash.php">Trash</a> 
        
 	<h2>All Book Record</h2>
 	<?php 
 		if (isset($_GET['msg'])) {
 			$msg = $_GET['msg'];
 			echo "<span style='color:green;font-size:20px'>".$msg."</span>";
 		}
 	 ?>
 	<table border="1px">
 		<tr>
 			<th>Sl</th>
 			<th>Title</th>
 			<th>Author</th>
 			<th>Actions</th>
 		</tr>
 		<?php 
 			$result = $bk->index();
 			if ($result) {
 				$i = 0;
 				while($alldata = $result->fetch_assoc()) {
 				$i++;
 		 ?>
 		<tr>
 			<td><?php echo $i; ?></td>
 			<td><?php echo $alldata['title']; ?></td>
 			<td><?php echo $alldata['author']; ?></td>
 			<td>
 				<a href="update.php?my_id=<?php echo $alldata['id']; ?>">Edit</a> || 
 				<!--<a href="trash.php?my_id=<?php echo $alldata['id']; ?>" onclick="return confirm('Are you sure to delete data?');">Trash</a>--> 
                                <a href="trash.php?my_id=<?php echo $alldata['id']; ?>">Trash</a> 
 			</td>
 		</tr>
 		<?php } } ?>
 	</table>
 </body>
 </html>