<?php 
	include_once 'config/config.php';
	class DB
	{
		private $host   = DB_HOST;
		private $user   = DB_USER;
		private $pass   = DB_PASS;
		private $dbname = DB_NAME;

		public $link;
		public $error;

		public function __construct()
		{
			$this->connectDB();
		}

		private function connectDB()
		{
			$this->link = new mysqli($this->host, $this->user, $this->pass, $this->dbname);
			if (!$this->link) {
				echo "Connection fail".$this->link->connect_error;
				return false;
			}
		}

		// Select or Read data
		public function select($query)
		{
			$result = $this->link->query($query) or die($this->link->error.__LINE__);
			if ($result->num_rows > 0) {
				return $result;
			} else {
				return false;
			}
		}
                
                public function select1($query)
		{
			$result = $this->link->query($query) or die($this->link->error.__LINE__);
			if ($result->num_rows > 0) {
				return $result;
			} else {
				return false;
			}
		}
                
                
                

		// Insert data
		public function insert($query)
		{
			$insert_row = $this->link->query($query) or die($this->link->error.__LINE__);
			if ($insert_row) {
				return $insert_row;
			} else {
				return false;
			}
		}

		// Update data
		public function update($query)
		{
			$upate_row = $this->link->query($query) or die($this->link->error.__LINE__);
			if ($upate_row) {
				return $upate_row;
			} else {
				return false;
			}
		}

		// Delete data
		public function trash($query)
		{
			$trash_row = $this->link->query($query) or die($this->link->error.__LINE__);
			if ($trash_row) {
				return $trash_row;
			} else {
				return false;
			}
		}
                
                
                public function restore($query)
		{
			$restore_row = $this->link->query($query) or die($this->link->error.__LINE__);
			if ($restore_row) {
				return $restore_row;
			} else {
				return false;
			}
		}
                
                public function emty_trash($query)
		{
			$delete_row = $this->link->query($query) or die($this->link->error.__LINE__);
			if ($delete_row) {
				return $delete_row;
			} else {
				return false;
			}
		}
	}
 ?>